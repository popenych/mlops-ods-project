# Mlops Ods Project

## Repository Methodology

This project follows a specific methodology for managing the repository:

1. **Linting and Formatting**: The project uses `ruff` and `mypy` to ensure code quality and consistent formatting. These tools are set up as pre-commit hooks, so they will automatically run when you try to commit your changes.

2. **Branching**: When working on a new feature or bug fix, create a new branch with a descriptive name (e.g., `feature/add-new-model`, `fix/incorrect-data-handling`). Avoid committing directly to the main branch.

3. **Commits**: Write clear and concise commit messages that describe the changes you've made. Follow the [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/) guidelines.

4. **Merge Requests**: When your changes are ready, create a merge request on GitLab. Provide a detailed description of the changes and reference any related issues.

5. **Documentation**: Keep the `CONTRIBUTING.md` file up-to-date with any changes to the development workflow or project requirements.
