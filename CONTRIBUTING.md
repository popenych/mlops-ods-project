# Contributing to MLOps DS Project

Thank you for your interest in contributing to this project! To ensure a smooth collaboration, please follow these guidelines.

## Development Environment

1. Create a conda environment and activate it:

```console
conda create -n mlops_env python=3.12
conda activate mlops_env
```

2. Install the required dependencies:

```console
conda install -c conda-forge ruff mypy pre-commit nbqa
```

## Linting and Formatting

This project uses the following tools for code quality and formatting:

- **ruff**: Checks code style and syntax, automatically formats the code and sorts imports
- **mypy**: Performs static type checking.
- **nbqa**: Run ruff and mypy on Jupyter Notebooks

Before committing your changes, please run the following commands to ensure your code meets the project's standards:

```console
ruff .
mypy .
```

The pre-commit hook is set up to automatically run these tools when you try to commit your changes. If the checks fail, you'll need to fix the issues before you can commit.

## Submitting Changes

1. Fork the repository.
2. Create a new branch for your changes.
3. Make your changes and commit them.
4. Push your changes to your fork.
5. Create a merge request on GitLab.

Thank you for your contribution!
